﻿using MillProfiles.Entity;
using MillProfiles.Models;

namespace MillProfiles.Repositories
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {

        public ProfileRepository(MillContext context)
            : base(context)
        {
        }

        public MillContext MillContext
        {
            get
            {
                return (MillContext as MillContext);
            }
        }
    }
}
