﻿using MillProfiles.Entity;
using MillProfiles.Models;

namespace MillProfiles.Repositories
{
    public class MillLogRepository : GenericRepository<Mill_Log>, IMillLogRepository
    {
        public MillLogRepository(MillContext context) : base(context)
        {
        }

        public MillContext MillContext
        {
            get
            {
                return (MillContext as MillContext);
            }
        }
    }
}
