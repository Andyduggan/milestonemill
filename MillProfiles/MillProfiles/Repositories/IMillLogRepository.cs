﻿using MillProfiles.Entity;
using MillProfiles.Models;

namespace MillProfiles.Repositories
{
    public interface IMillLogRepository : IGenericRepository<Mill_Log>
    {
        MillContext MillContext { get; }
    }
}
