﻿using MillProfiles.Entity;
using MillProfiles.Models;

namespace MillProfiles.Repositories
{
    public interface IProfileRepository : IGenericRepository<Profile>
    {
        MillContext MillContext { get; }
    }
}
