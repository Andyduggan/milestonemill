﻿namespace MillProfiles.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addcustomercode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.profile", "cust_code", c => c.String(maxLength: 3, storeType: "nvarchar"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.profile", "cust_code");
        }
    }
}
