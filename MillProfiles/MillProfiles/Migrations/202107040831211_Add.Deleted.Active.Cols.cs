﻿namespace MillProfiles.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDeletedActiveCols : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.profile", "is_deleted", c => c.Int(nullable: false));
            AddColumn("dbo.profile", "is_active", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.profile", "is_active");
            DropColumn("dbo.profile", "is_deleted");
        }
    }
}
