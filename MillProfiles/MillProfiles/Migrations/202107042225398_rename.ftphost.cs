﻿namespace MillProfiles.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameftphost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.profile", "ftp_host", c => c.String(maxLength: 100, storeType: "nvarchar"));
            DropColumn("dbo.profile", "fto_host");
        }
        
        public override void Down()
        {
            AddColumn("dbo.profile", "fto_host", c => c.String(maxLength: 100, storeType: "nvarchar"));
            DropColumn("dbo.profile", "ftp_host");
        }
    }
}
