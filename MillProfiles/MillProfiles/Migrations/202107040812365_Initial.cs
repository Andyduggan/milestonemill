﻿namespace MillProfiles.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.log",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        log_date = c.DateTime(nullable: false, precision: 0),
                        profile_id = c.Int(nullable: false),
                        file_name = c.String(maxLength: 100, storeType: "nvarchar"),
                        result = c.String(maxLength: 10, storeType: "nvarchar"),
                        message = c.String(maxLength: 1000, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.profile",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        customer_name = c.String(maxLength: 100, storeType: "nvarchar"),
                        cw_code = c.String(maxLength: 3, storeType: "nvarchar"),
                        eadpator_url = c.String(maxLength: 100, storeType: "nvarchar"),
                        eadaptor_user = c.String(maxLength: 20, storeType: "nvarchar"),
                        eadaptor_pass = c.String(maxLength: 20, storeType: "nvarchar"),
                        fto_host = c.String(maxLength: 100, storeType: "nvarchar"),
                        ftp_user = c.String(maxLength: 100, storeType: "nvarchar"),
                        ftp_pass = c.String(maxLength: 100, storeType: "nvarchar"),
                        ftp_path = c.String(maxLength: 100, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.profile");
            DropTable("dbo.log");
        }
    }
}
