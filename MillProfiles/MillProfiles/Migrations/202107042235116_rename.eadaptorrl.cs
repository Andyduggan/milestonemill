﻿namespace MillProfiles.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameeadaptorrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.profile", "eadaptor_url", c => c.String(maxLength: 100, storeType: "nvarchar"));
            DropColumn("dbo.profile", "eadpator_url");
        }
        
        public override void Down()
        {
            AddColumn("dbo.profile", "eadpator_url", c => c.String(maxLength: 100, storeType: "nvarchar"));
            DropColumn("dbo.profile", "eadaptor_url");
        }
    }
}
