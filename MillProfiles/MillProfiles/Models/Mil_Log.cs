﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MillProfiles.Models
{
    public class Mill_Log
    {
        [Key]
        public int id { get; set; }
        public DateTime log_date { get; set; }
        public int profile_id { get; set; }
        [MaxLength(100)]
        public string file_name { get; set; }
        [MaxLength(10)]
        public string result { get; set; }
        [MaxLength(1000)]
        public string message { get; set; }

    }
}
