﻿using System.ComponentModel.DataAnnotations;

namespace MillProfiles.Models
{
    public class Profile
    {

        [Key]
        public int id { get; set; }
        [MaxLength(100)]
        public string customer_name { get; set; }
        [MaxLength(3)]
        public string cust_code { get; set; }
        [MaxLength(3)]
        public string cw_code { get; set; }
        [MaxLength(100)]
        public string eadaptor_url { get; set; }
        [MaxLength(20)]
        public string eadaptor_user { get; set; }
        [MaxLength(20)]
        public string eadaptor_pass { get; set; }
        [MaxLength(100)]
        public string ftp_host { get; set; }
        [MaxLength(100)]
        public string ftp_user { get; set; }
        [MaxLength(100)]
        public string ftp_pass { get; set; }
        [MaxLength(100)]
        public string ftp_path { get; set; }

        public int is_deleted { get; set; }

        public int is_active { get; set; }


    }
}
