﻿using MillProfiles.Entity;
using MillProfiles.Repositories;
using System;
using System.Data.Entity.Validation;

namespace MillProfiles.DAL
{
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        #region members
        private bool disposed = false;
        private readonly MillContext _context;
        private string _errMsgs;
        #endregion
        #region properties
        public IProfileRepository Profiles
        {
            get;
            private set;
        }
        public IMillLogRepository Logs
        {
            get;
            private set;
        }

        #endregion

        #region helpers
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposed = true;
            }
        }
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        #endregion
        #region constructors
        public UnitOfWork(MillContext context)
        {
            this._context = context;
            Logs = new MillLogRepository(context);
            Profiles = new ProfileRepository(context);
        }

        #endregion

        #region methods
        public int Complete()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        _errMsgs += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage) + Environment.NewLine;

                    }
                }
                throw new Exception(_errMsgs, ex);
                return 0;
            }
        }
        #endregion
    }
}
