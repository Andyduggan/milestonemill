﻿using MillProfiles.Repositories;
using System;

namespace MillProfiles.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IMillLogRepository Logs { get; }
        IProfileRepository Profiles { get; }

        int Complete();
        void Dispose();
    }
}