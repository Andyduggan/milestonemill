﻿using MillProfiles.Models;
using System.Data.Entity;

namespace MillProfiles.Entity
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class MillContext : DbContext
    {
    
        public MillContext()
            : base("server=localhost;port=3306;database=MilestoneMill;uid=millAdmin;password=ftSupermill!2021")
        {
            Database.SetInitializer<MillContext>(new MySqlDbInitializer());
        }

        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Mill_Log> Logs { get; set; }
   
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Profile>()
                 .ToTable("profile");
            modelBuilder.Entity<Mill_Log>().ToTable("log");
        }
    }

    public class MySqlDbInitializer : CreateDatabaseIfNotExists<MillContext>
    {
        protected override void Seed(MillContext context)
        {
            base.Seed(context);
        }
    }


}
