﻿using MillProfiles.DAL;
using MillProfiles.Entity;
using MillProfiles.Models;
using MillProfiles.Resources;
using System;
using System.Linq;
using System.Windows.Forms;

namespace MillProfiles
{
    public partial class Start : Form
    {
        int profileId = 0;
        public Start()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            foreach (var control in groupBox1.Controls.OfType<TextBox>())
            {
                control.Text = "";
            }
            cbIsActive.Checked = true;
            textProfileName.Focus();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new MillContext()))
            {
                Profile profile = new Profile();
                if (profileId>0)
                {
                    profile =  uow.Profiles.GetById(profileId);
                }
               
                profile.customer_name = textProfileName.Text;
                profile.cust_code = textFTCode.Text;
                profile.cw_code = textCWCode.Text;
                profile.eadaptor_url = texteAdaptorURL.Text;
                profile.eadaptor_user = textEadaptorUser.Text;
                profile.eadaptor_pass = textEadaptorPass.Text;
                profile.ftp_host = textFTPServer.Text;
                profile.ftp_pass = textFtpPass.Text;
                profile.ftp_user = textFtpUser.Text;
                profile.ftp_path = textFtpPath.Text;
                profile.is_active = cbIsActive.Checked ? 1 : 0;
                profile.is_deleted = 0;
                if (profile.id == 0)
                {
                    uow.Profiles.Add(profile);
                }
                uow.Complete();
                profileId = 0;

            }
            FillGrid();
        }

        private void FillGrid()
        {
            grdProfiles.AutoGenerateColumns = false;
            var bsProfile = new BindingSource();
            using (IUnitOfWork uow = new UnitOfWork(new MillContext()))
            {
                var profiles = uow.Profiles.Find(x => x.is_active == 1).OrderBy(y => y.customer_name).ToList();
                var sortedBS = new SortableBindingList<Profile>(profiles);
                bsProfile.DataSource = sortedBS;
                grdProfiles.DataSource = bsProfile;

            }
        }

        private void Start_Load(object sender, EventArgs e)
        {
            FillGrid();
        }

        private void grdProfiles_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var row = grdProfiles.SelectedRows[e.RowIndex];
                profileId = (int)row.Cells["id"].Value;
            }
            GetProfile(profileId);

        }

        private void GetProfile(int profileId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new MillContext()))
            {
                var profile = uow.Profiles.GetById(profileId);
                if (profile != null)
                {
                    textProfileName.Text = profile.customer_name;
                    textCWCode.Text = profile.cw_code;
                    textFTCode.Text = profile.cust_code;
                    texteAdaptorURL.Text = profile.eadaptor_url;
                    textEadaptorUser.Text = profile.eadaptor_user;
                    textEadaptorPass.Text = profile.eadaptor_pass;
                    textFTPServer.Text = profile.ftp_host;
                    textFtpUser.Text = profile.ftp_user;
                    textFtpPass.Text = profile.ftp_pass;
                    textFtpPath.Text = profile.ftp_path;
                    cbIsActive.Checked = profile.is_active == 1 ? true : false;
                }
            }
        }
    }
}
