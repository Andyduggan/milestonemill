﻿
namespace MillProfiles
{
    partial class Start
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Start));
            this.btnClose = new System.Windows.Forms.Button();
            this.grdProfiles = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.textFtpPath = new System.Windows.Forms.TextBox();
            this.textFtpPass = new System.Windows.Forms.TextBox();
            this.textFtpUser = new System.Windows.Forms.TextBox();
            this.textFTPServer = new System.Windows.Forms.TextBox();
            this.textEadaptorPass = new System.Windows.Forms.TextBox();
            this.textEadaptorUser = new System.Windows.Forms.TextBox();
            this.texteAdaptorURL = new System.Windows.Forms.TextBox();
            this.textCWCode = new System.Windows.Forms.TextBox();
            this.textFTCode = new System.Windows.Forms.TextBox();
            this.textProfileName = new System.Windows.Forms.TextBox();
            this.cbIsActive = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.profileName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ftCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cwCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ePass = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.grdProfiles)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(724, 481);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(63, 34);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grdProfiles
            // 
            this.grdProfiles.AllowUserToAddRows = false;
            this.grdProfiles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProfiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.profileName,
            this.ftCode,
            this.cwCode,
            this.eUrl,
            this.eUser,
            this.ePass});
            this.grdProfiles.Location = new System.Drawing.Point(13, 13);
            this.grdProfiles.Name = "grdProfiles";
            this.grdProfiles.ReadOnly = true;
            this.grdProfiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdProfiles.Size = new System.Drawing.Size(775, 260);
            this.grdProfiles.TabIndex = 1;
            this.grdProfiles.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdProfiles_CellContentDoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNew);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.textFtpPath);
            this.groupBox1.Controls.Add(this.textFtpPass);
            this.groupBox1.Controls.Add(this.textFtpUser);
            this.groupBox1.Controls.Add(this.textFTPServer);
            this.groupBox1.Controls.Add(this.textEadaptorPass);
            this.groupBox1.Controls.Add(this.textEadaptorUser);
            this.groupBox1.Controls.Add(this.texteAdaptorURL);
            this.groupBox1.Controls.Add(this.textCWCode);
            this.groupBox1.Controls.Add(this.textFTCode);
            this.groupBox1.Controls.Add(this.textProfileName);
            this.groupBox1.Controls.Add(this.cbIsActive);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(16, 279);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(783, 196);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Profile";
            // 
            // btnNew
            // 
            this.btnNew.Image = ((System.Drawing.Image)(resources.GetObject("btnNew.Image")));
            this.btnNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNew.Location = new System.Drawing.Point(714, 135);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(58, 26);
            this.btnNew.TabIndex = 22;
            this.btnNew.Text = "&New";
            this.btnNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(714, 164);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(58, 26);
            this.btnSave.TabIndex = 21;
            this.btnSave.Text = "&Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // textFtpPath
            // 
            this.textFtpPath.Location = new System.Drawing.Point(74, 163);
            this.textFtpPath.MaxLength = 100;
            this.textFtpPath.Name = "textFtpPath";
            this.textFtpPath.Size = new System.Drawing.Size(373, 20);
            this.textFtpPath.TabIndex = 20;
            // 
            // textFtpPass
            // 
            this.textFtpPass.Location = new System.Drawing.Point(282, 134);
            this.textFtpPass.MaxLength = 100;
            this.textFtpPass.Name = "textFtpPass";
            this.textFtpPass.Size = new System.Drawing.Size(373, 20);
            this.textFtpPass.TabIndex = 19;
            // 
            // textFtpUser
            // 
            this.textFtpUser.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.textFtpUser.Location = new System.Drawing.Point(74, 134);
            this.textFtpUser.MaxLength = 20;
            this.textFtpUser.Name = "textFtpUser";
            this.textFtpUser.Size = new System.Drawing.Size(108, 20);
            this.textFtpUser.TabIndex = 18;
            // 
            // textFTPServer
            // 
            this.textFTPServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textFTPServer.Location = new System.Drawing.Point(90, 105);
            this.textFTPServer.MaxLength = 100;
            this.textFTPServer.Name = "textFTPServer";
            this.textFTPServer.Size = new System.Drawing.Size(373, 20);
            this.textFTPServer.TabIndex = 17;
            // 
            // textEadaptorPass
            // 
            this.textEadaptorPass.Location = new System.Drawing.Point(307, 76);
            this.textEadaptorPass.MaxLength = 20;
            this.textEadaptorPass.Name = "textEadaptorPass";
            this.textEadaptorPass.Size = new System.Drawing.Size(156, 20);
            this.textEadaptorPass.TabIndex = 16;
            // 
            // textEadaptorUser
            // 
            this.textEadaptorUser.Location = new System.Drawing.Point(103, 76);
            this.textEadaptorUser.MaxLength = 20;
            this.textEadaptorUser.Name = "textEadaptorUser";
            this.textEadaptorUser.Size = new System.Drawing.Size(116, 20);
            this.textEadaptorUser.TabIndex = 15;
            // 
            // texteAdaptorURL
            // 
            this.texteAdaptorURL.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.texteAdaptorURL.Location = new System.Drawing.Point(256, 47);
            this.texteAdaptorURL.MaxLength = 100;
            this.texteAdaptorURL.Name = "texteAdaptorURL";
            this.texteAdaptorURL.Size = new System.Drawing.Size(360, 20);
            this.texteAdaptorURL.TabIndex = 14;
            // 
            // textCWCode
            // 
            this.textCWCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textCWCode.Location = new System.Drawing.Point(103, 47);
            this.textCWCode.MaxLength = 3;
            this.textCWCode.Name = "textCWCode";
            this.textCWCode.Size = new System.Drawing.Size(45, 20);
            this.textCWCode.TabIndex = 13;
            // 
            // textFTCode
            // 
            this.textFTCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textFTCode.Location = new System.Drawing.Point(593, 17);
            this.textFTCode.MaxLength = 3;
            this.textFTCode.Name = "textFTCode";
            this.textFTCode.Size = new System.Drawing.Size(46, 20);
            this.textFTCode.TabIndex = 12;
            // 
            // textProfileName
            // 
            this.textProfileName.Location = new System.Drawing.Point(90, 18);
            this.textProfileName.MaxLength = 100;
            this.textProfileName.Name = "textProfileName";
            this.textProfileName.Size = new System.Drawing.Size(373, 20);
            this.textProfileName.TabIndex = 11;
            // 
            // cbIsActive
            // 
            this.cbIsActive.AutoSize = true;
            this.cbIsActive.Location = new System.Drawing.Point(656, 18);
            this.cbIsActive.Name = "cbIsActive";
            this.cbIsActive.Size = new System.Drawing.Size(67, 17);
            this.cbIsActive.TabIndex = 10;
            this.cbIsActive.Text = "Is Active";
            this.cbIsActive.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 166);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "FTP Path";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(200, 138);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "FTP Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "FTP User";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "FTP Server";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(225, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "eAdaptor Pass";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "eAdaptor User";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(169, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cargowise URL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(483, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "FreightTracker Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cargowise Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Profile Name";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // profileName
            // 
            this.profileName.DataPropertyName = "customer_name";
            this.profileName.HeaderText = "Name";
            this.profileName.Name = "profileName";
            this.profileName.ReadOnly = true;
            // 
            // ftCode
            // 
            this.ftCode.DataPropertyName = "cust_code";
            this.ftCode.HeaderText = "Cust Code";
            this.ftCode.Name = "ftCode";
            this.ftCode.ReadOnly = true;
            // 
            // cwCode
            // 
            this.cwCode.DataPropertyName = "cw_code";
            this.cwCode.HeaderText = "CW Code";
            this.cwCode.Name = "cwCode";
            this.cwCode.ReadOnly = true;
            // 
            // eUrl
            // 
            this.eUrl.DataPropertyName = "eadaptor_url";
            this.eUrl.HeaderText = "eAdapter URL";
            this.eUrl.Name = "eUrl";
            this.eUrl.ReadOnly = true;
            // 
            // eUser
            // 
            this.eUser.DataPropertyName = "eadaptor_user";
            this.eUser.HeaderText = "eAdpater User";
            this.eUser.Name = "eUser";
            this.eUser.ReadOnly = true;
            // 
            // ePass
            // 
            this.ePass.DataPropertyName = "eadaptor_pass";
            this.ePass.HeaderText = "eAdapter Pass";
            this.ePass.Name = "ePass";
            this.ePass.ReadOnly = true;
            // 
            // Start
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 527);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grdProfiles);
            this.Controls.Add(this.btnClose);
            this.Name = "Start";
            this.Text = "FreightTracker Milestone Mill (Profiles)";
            this.Load += new System.EventHandler(this.Start_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdProfiles)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView grdProfiles;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cbIsActive;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox textFtpPath;
        private System.Windows.Forms.TextBox textFtpPass;
        private System.Windows.Forms.TextBox textFtpUser;
        private System.Windows.Forms.TextBox textFTPServer;
        private System.Windows.Forms.TextBox textEadaptorPass;
        private System.Windows.Forms.TextBox textEadaptorUser;
        private System.Windows.Forms.TextBox texteAdaptorURL;
        private System.Windows.Forms.TextBox textCWCode;
        private System.Windows.Forms.TextBox textFTCode;
        private System.Windows.Forms.TextBox textProfileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn profileName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ftCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn cwCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn eUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn eUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn ePass;
    }
}