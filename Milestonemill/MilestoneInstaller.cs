﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Milestonemill
{
    [RunInstaller(true)]
    public partial class MilestoneInstaller : System.Configuration.Install.Installer
    {
        public MilestoneInstaller()
        {
            InitializeComponent();
        }
    }
}
