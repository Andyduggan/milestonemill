﻿using Milestonemill.Models;
using System;
using System.Reflection;
using System.ServiceProcess;

namespace Milestonemill
{
    partial class MilestoneService : ServiceBase
    {
        public MilestoneService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var m = MethodBase.GetCurrentMethod();
            Library.ReadPreferences();
            Library.WriteLog(new Log
            {
                DateTime = DateTime.Now,
                ProfileId = "System",
                Error = "Starting",
                Message = "Service Starting",
                Process = m.Name
            });
            Library.TmrMain = new System.Timers.Timer();
            Library.TmrMain.Interval = Library.PollTime;
            Library.TmrMain.Elapsed += new System.Timers.ElapsedEventHandler(Library.OnElapsedTime);
            Library.TmrMain.Enabled = true;
        }

        protected override void OnStop()
        {
            var m = MethodBase.GetCurrentMethod();
            Library.WriteLog(new Log
            {
                DateTime = DateTime.Now,
                ProfileId = "System",
                Error = "Stopping",
                Message = "Service Shutting Down",
                Process = m.Name
            });
        }
    }
}
