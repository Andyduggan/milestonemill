﻿
namespace Milestonemill
{
    partial class MilestoneInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mmServiceProcessor = new System.ServiceProcess.ServiceProcessInstaller();
            this.mmServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // mmServiceProcessor
            // 
            this.mmServiceProcessor.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.mmServiceProcessor.Password = null;
            this.mmServiceProcessor.Username = null;
            // 
            // mmServiceInstaller
            // 
            this.mmServiceInstaller.Description = "Milestone Mill Worker service";
            this.mmServiceInstaller.DisplayName = "Milestone Mill";
            this.mmServiceInstaller.ServiceName = "MilestoneService";
            // 
            // MilestoneInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.mmServiceProcessor,
            this.mmServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller mmServiceProcessor;
        private System.ServiceProcess.ServiceInstaller mmServiceInstaller;
    }
}