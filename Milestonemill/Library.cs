﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Milestonemill.Models;
using Milestonemill.Resources;
using MimeKit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;
using System.Xml.Linq;
using WinSCP;

namespace Milestonemill
{
    public static class Library
    {
        static MailSvr mailServer { get; set; }
        public static string LogFile { get; set; }
        public static string LogLocation { get; set; }
        public static string Preferences { get; set; }
        public static string Profiles { get; set; }
        public static Timer TmrMain { get; set; }
        public static int PollTime { get; set; }
        public const string AppPath = @"C:\30KFT";
        public static string WorkingDir = Path.Combine(AppPath, "Working");
        static string supportEmail = "";


        // This class will house the Processes to handle the various functions. 



        //TODO: Create a Timer and Timer Elapsed Event
        internal static void OnElapsedTime(object sender, ElapsedEventArgs e)
        {
            TmrMain.Stop();
            ReadPreferences();
            PollProfiles();
            TmrMain.Start();
        }

        // This method is what will loop through all of the Profiles looking for Milestones to be sent
        private static void PollProfiles()
        {
            var profiles = GetAllProfiles(true);
            foreach (var p in profiles)
            {
                GetFiles(p);
                ProcessFiles(p);
            }
        }

        private static void ProcessFiles(Profile P)
        {
            var m = MethodInfo.GetCurrentMethod();
            var files = Directory.GetFiles(WorkingDir);
            foreach (var file in files)
            {
                var fileSent = SendCWFile(file, P.EAUrl, P.User, P.Password, P.CWCode);
                if (fileSent.Contains("Ok"))
                {
                    WriteLog(new Log
                    {
                        DateTime = DateTime.Now,
                        FileName = file,
                        Process = m.Name,
                        Message = fileSent,
                        ProfileId = P.Client
                    }, Path.Combine(LogLocation, P.LogName));
                    SendFile(file, P);

                }
                else
                {
                    WriteLog(new Log
                    {
                        DateTime = DateTime.Now,
                        FileName = file,
                        Process = m.Name,
                        Message = fileSent,
                        Error = "Unable to Send to eAdapter",
                        ProfileId = P.Client
                    }, Path.Combine(LogLocation, P.LogName));
                    SendMessage("Error sending File", supportEmail, fileSent, file);
                }
                File.Delete(file);
            }



        }

        //Creates an entry into the Log XML
        public static void WriteLog(Log log)
        {

            if (!File.Exists(LogFile))
            {
                XDocument xnewLog = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                new XElement("Logs",
                                                    new XElement("Log",
                                                        new XElement("ID"),
                                                        new XElement("DateTime"),
                                                        new XElement("ProfileID"),
                                                        new XElement("FileName"),
                                                        new XElement("Process"),
                                                        new XElement("Result"),
                                                        new XElement("Message")
                                                                )
                                                            )
                                                );
                xnewLog.Save(LogFile);
            }

            XDocument xLog = XDocument.Load(LogFile);
            XElement ele = new XElement("Log",
                                                new XElement("ProfileID", log.ProfileId),
                                               new XElement("DateTime", log.DateTime.ToString("G")),
                                               new XElement("FileName", log.FileName),
                                               new XElement("Process", log.Process),
                                               new XElement("Result", log.Error),
                                               new XElement("Message", log.Message));
            xLog.Root.Add(ele);
            xLog.Save(LogFile);
        }
        //This is the Customer Specific Logging function
        public static void WriteLog(Log log, string logFile)
        {

            if (!File.Exists(logFile))
            {
                XDocument xnewLog = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                new XElement("Logs",
                                                    new XElement("Log",
                                                        new XElement("ID"),
                                                        new XElement("DateTime"),
                                                        new XElement("ProfileID"),
                                                        new XElement("FileName"),
                                                        new XElement("Process"),
                                                        new XElement("Result"),
                                                        new XElement("Message")
                                                                )
                                                            )
                                                );
                xnewLog.Save(LogFile);
            }

            XDocument xLog = XDocument.Load(LogFile);
            XElement ele = new XElement("Log",
                                                new XElement("ProfileID", log.ProfileId),
                                               new XElement("DateTime", log.DateTime.ToString("G")),
                                               new XElement("FileName", log.FileName),
                                               new XElement("Process", log.Process),
                                               new XElement("Result", log.Error),
                                               new XElement("Message", log.Message));
            xLog.Root.Add(ele);
            xLog.Save(logFile);
        }

        //TODO: Create a Mail (SMTP) service to notify of any failures
        public static void SendMessage(string subject, string recip, string msg, string att)
        {
            var m = MethodInfo.GetCurrentMethod();
            try
            {
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress("milestonemill@freighttracker.com.au"));
                message.To.Add(new MailboxAddress(supportEmail));
                message.Subject = subject;
                var body = new BodyBuilder();
                body.Attachments.Add(att);
                body.TextBody = msg;
                message.Body = body.ToMessageBody();
                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(mailServer.Server, int.Parse(mailServer.Port), SecureSocketOptions.SslOnConnect);
                    smtp.Authenticate(mailServer.User, mailServer.Password);
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                WriteLog(new Log
                {
                    DateTime = DateTime.Now,
                    FileName = att,
                    Process = m.Name,
                    Message = ex.GetType().Name + " Error: ",
                    Error = ex.Message,
                    ProfileId = "System"
                });
            }



        }
        // Lookup Profile based on ID
        public static Profile GetProfile(string id)
        {
            var profList = XDocument.Load(Profiles);
            return (from x in profList.Elements("Profile")
                    where x.Element("ID").Value == id
                    select new Profile
                    {
                        Customer = x.Element("Customer").Value,
                        Client = x.Element("Client").Value,
                        Context = x.Element("CargowiseContext").Value,
                        CWCode = x.Element("OrgCode").Value,
                        Server = x.Element("CargowiseServer").Value,
                        User = x.Element("eAdapterUsername").Value,
                        Password = x.Element("eAdapterPassword").Value,
                        EAUrl = x.Element("eAdapterURL").Value,
                        FTPServer = x.Element("FTPServer").Value,
                        FTPUser = x.Element("FTPUserName").Value,
                        FTPPassword = x.Element("FTPPassword").Value,
                        FTPPath = x.Element("FTPPath").Value,
                        ProcessedPath = x.Element("ProcessedPath").Value,
                        Active = int.Parse(x.Element("Active").Value.ToString()),
                        LogName = x.Element("LogName").Value
                    }).FirstOrDefault();
        }
        // Return all profiles with specified Active 
        public static List<Profile> GetAllProfiles(bool active)
        {
            var profiles = new List<Profile>();
            var profList = XDocument.Load(Profiles);
            string filt = active ? "1" : "0";
            profiles = (from x in profList.Elements("Profile")
                        where x.Element("Active").Value == filt
                        select new Profile
                        {
                            Customer = x.Element("Customer").Value,
                            Client = x.Element("Client").Value,
                            Context = x.Element("CargowiseContext").Value,
                            CWCode = x.Element("OrgCode").Value,
                            Server = x.Element("CargowiseServer").Value,
                            User = x.Element("eAdapterUsername").Value,
                            Password = x.Element("eAdapterPassword").Value,
                            EAUrl = x.Element("eAdapterURL").Value,
                            FTPServer = x.Element("FTPServer").Value,
                            FTPUser = x.Element("FTPUserName").Value,
                            FTPPassword = x.Element("FTPPassword").Value,
                            FTPPath = x.Element("FTPPath").Value,
                            ProcessedPath = x.Element("ProcessedPath").Value,
                            Active = int.Parse(x.Element("Active").Value.ToString()),
                            LogName = x.Element("LogName").Value
                        }).ToList();
            return profiles;
        }
        //Return all Profiles.
        public static List<Profile> GetAllProfiles()
        {
            var profiles = new List<Profile>();
            var profList = XDocument.Load(Profiles);
            profiles = (from x in profList.Elements("Profile")
                        select new Profile
                        {
                            Customer = x.Element("Customer").Value,
                            Client = x.Element("Client").Value,
                            Context = x.Element("CargowiseContext").Value,
                            CWCode = x.Element("OrgCode").Value,
                            Server = x.Element("CargowiseServer").Value,
                            User = x.Element("eAdapterUsername").Value,
                            Password = x.Element("eAdapterPassword").Value,
                            EAUrl = x.Element("eAdapterURL").Value,
                            FTPServer = x.Element("FTPServer").Value,
                            FTPUser = x.Element("FTPUserName").Value,
                            FTPPassword = x.Element("FTPPassword").Value,
                            FTPPath = x.Element("FTPPath").Value,
                            ProcessedPath = x.Element("ProcessedPath").Value,
                            Active = int.Parse(x.Element("Active").Value.ToString()),
                            LogName = x.Element("LogName").Value
                        }).ToList();


            return profiles;
        }

        //Reads the Files from the FTP Server
        static void GetFiles(Profile profile)
        {
            var m = MethodInfo.GetCurrentMethod();
            try
            {
                if (!Directory.Exists(WorkingDir))
                {
                    Directory.CreateDirectory(WorkingDir);
                }
                SessionOptions sessionOptions = new SessionOptions
                {
                    HostName = profile.FTPServer,
                    UserName = profile.FTPUser,
                    Password = profile.FTPPassword,
                    PortNumber = 21,
                    Protocol = Protocol.Ftp
                };

                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    TransferOperationResult transferResult;
                    transferResult =
                        session.GetFiles(profile.FTPPath, WorkingDir, true, transferOptions);
                    // Throw on any error
                    transferResult.Check();
                }
            }
            catch (Exception ex)
            {
                WriteLog(new Log
                {
                    DateTime = DateTime.Now,
                    Process = m.Name,
                    Message = ex.GetType().Name + " Error: ",
                    Error = ex.Message,
                    ProfileId = "System"
                });
            }

        }
        //TODO: Create a FTP Send Function - To put the processed Milestones
        static void SendFile(string fileToSend, Profile profile)
        {
            var m = MethodInfo.GetCurrentMethod();
            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    HostName = profile.FTPServer,
                    UserName = profile.FTPUser,
                    Password = profile.FTPPassword,
                    PortNumber = 21,
                    Protocol = Protocol.Ftp
                };
                using (Session session = new Session())
                {
                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.OverwriteMode = OverwriteMode.Overwrite;
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult =
                        session.PutFiles(fileToSend, CheckPath(profile.ProcessedPath) + Path.GetFileName(fileToSend), false, transferOptions);
                    // Throw on any error
                    transferResult.Check();
                }
            }
            catch (Exception ex)
            {
                WriteLog(new Log
                {
                    DateTime = DateTime.Now,
                    FileName = fileToSend,
                    Process = m.Name,
                    Message = ex.GetType().Name + " Error: ",
                    Error = ex.Message,
                    ProfileId = "System"
                });
            }

        }

        static string CheckPath(string path)
        {
            var s = (from t in path
                     select t).Last();
            if (s != '/')
            {
                path = path + "/";
            }
            return path;
        }

        //Sends the File to eAdaptor
        static string SendCWFile(string fileName, string eUrl, string user, string password, string cwCode)
        {
            string result = "";
            try
            {
                eAdaptor.SendMessage(
                    eUrl,
                    fileName,
                    cwCode,
                    user,
                    password);
                result = Path.GetFileName(fileName) + " send Ok";

            }
            catch (Exception ex)
            {
                result = Path.GetFileName(fileName) + " Failed: " + ex.GetType().Name + ": " + ex.Message;
            }
            return result;
        }

        //Read the Preferences XML file
        public static void ReadPreferences()
        {

            if (!Directory.Exists(AppPath))
            {
                Directory.CreateDirectory(AppPath);
            }
            if (!File.Exists(Path.Combine(AppPath, "ProfileManager.XML")))
            {
                XDocument prefs = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                    new XElement("Preferences",
                                                    new XElement("Profiles"),
                                                    new XElement("LogPath"),
                                                    new XElement("WinSCPLocation"),
                                                    new XElement("PollTime"),
                                                    new XElement("SupportEmail"),
                                                    new XElement("MailServer"),
                                                    new XElement("Port"),
                                                    new XElement("SenderEmail"),
                                                    new XElement("UserName"),
                                                    new XElement("Password"),
                                                    new XElement("ServerOptions")));
                prefs.Save(Path.Combine(AppPath, "ProfileManager.XML"));
            }
            XDocument pmPrefs = XDocument.Load(Path.Combine(AppPath, "ProfileManager.XML"));
            var settings = (from p in pmPrefs.Descendants("Preferences")
                            select p).FirstOrDefault();
            LogFile = Path.Combine(settings.Element("LogPath").Value, "MileStoneMillLog.XML");
            LogLocation = settings.Element("LogPath").Value;
            Profiles = settings.Element("Profiles").Value;
            mailServer = new MailSvr
            {
                MailFrom = settings.Element("SenderEmail").Value,
                Server = settings.Element("MailServer").Value,
                Port = settings.Element("Port").Value,
                User = settings.Element("UserName").Value,
                Password = settings.Element("Password").Value,
                MailOption = settings.Element("ServerOptions").Value
            };
            supportEmail = settings.Element("SupportEmail").Value;
            int poll = 0;
            if (int.TryParse(settings.Element("PollTime").Value, out poll))
            {
                PollTime = poll;
            }
            else
            {
                PollTime = 5;
            }


        }
        //TODO:
        public class MailSvr
        {
            public string Server { get; set; }
            public string MailOption { get; set; }
            public string Port { get; set; }
            public string MailFrom { get; set; }
            public string User { get; set; }
            public string Password { get; set; }

            public MailSvr()
            {

            }
        }
    }
}
