﻿using System;

namespace Milestonemill.Models
{
    public class Log
    {
        public DateTime DateTime { get; set; }

        public string ProfileId { get; set; }
        public string FileName { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
        public string Process { get; set; }

    }
}
