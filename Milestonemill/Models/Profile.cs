﻿namespace Milestonemill.Models
{
    public class Profile
    {
        public string ID { get; set; }
        public string Customer { get; set; }
        public string Client { get; set; }
        public string Context { get; set; }
        public string Server { get; set; }
        public string CWCode { get; set; }
        public string FTPServer { get; set; }
        public string FTPUser { get; set; }
        public string FTPPassword { get; set; }
        public string FTPPath { get; set; }
        public string ProcessedPath { get; set; }
        public string EAUrl { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Active { get; set; }
        public string LogName { get; set; }
    }
}
