﻿using System.ServiceProcess;

namespace Milestonemill
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MilestoneService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
