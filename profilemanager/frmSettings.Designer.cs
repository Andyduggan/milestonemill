﻿
namespace profilemanager
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.btnClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbNone = new System.Windows.Forms.RadioButton();
            this.rbTls = new System.Windows.Forms.RadioButton();
            this.rbSsl = new System.Windows.Forms.RadioButton();
            this.edLog = new System.Windows.Forms.TextBox();
            this.edWinSCP = new System.Windows.Forms.TextBox();
            this.btnLog = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.edMailServer = new System.Windows.Forms.TextBox();
            this.edUsername = new System.Windows.Forms.TextBox();
            this.edPassword = new System.Windows.Forms.TextBox();
            this.edEmail = new System.Windows.Forms.TextBox();
            this.edSupport = new System.Windows.Forms.TextBox();
            this.edPort = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.edPollTime = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(448, 244);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "System Log";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "WinSCP Location";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Mail Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "SMTP Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "SMTP Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(99, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Email Address From";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Support Email Address";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Mail Server Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbNone);
            this.groupBox1.Controls.Add(this.rbTls);
            this.groupBox1.Controls.Add(this.rbSsl);
            this.groupBox1.Location = new System.Drawing.Point(173, 214);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(167, 44);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mail Server Settings";
            // 
            // rbNone
            // 
            this.rbNone.AutoSize = true;
            this.rbNone.Location = new System.Drawing.Point(112, 19);
            this.rbNone.Name = "rbNone";
            this.rbNone.Size = new System.Drawing.Size(51, 17);
            this.rbNone.TabIndex = 2;
            this.rbNone.TabStop = true;
            this.rbNone.Text = "None";
            this.rbNone.UseVisualStyleBackColor = true;
            // 
            // rbTls
            // 
            this.rbTls.AutoSize = true;
            this.rbTls.Location = new System.Drawing.Point(61, 19);
            this.rbTls.Name = "rbTls";
            this.rbTls.Size = new System.Drawing.Size(45, 17);
            this.rbTls.TabIndex = 1;
            this.rbTls.TabStop = true;
            this.rbTls.Text = "TLS";
            this.rbTls.UseVisualStyleBackColor = true;
            // 
            // rbSsl
            // 
            this.rbSsl.AutoSize = true;
            this.rbSsl.Location = new System.Drawing.Point(10, 19);
            this.rbSsl.Name = "rbSsl";
            this.rbSsl.Size = new System.Drawing.Size(45, 17);
            this.rbSsl.TabIndex = 0;
            this.rbSsl.TabStop = true;
            this.rbSsl.Text = "SSL";
            this.rbSsl.UseVisualStyleBackColor = true;
            // 
            // edLog
            // 
            this.edLog.Location = new System.Drawing.Point(127, 13);
            this.edLog.Name = "edLog";
            this.edLog.Size = new System.Drawing.Size(320, 20);
            this.edLog.TabIndex = 10;
            // 
            // edWinSCP
            // 
            this.edWinSCP.Location = new System.Drawing.Point(127, 40);
            this.edWinSCP.Name = "edWinSCP";
            this.edWinSCP.Size = new System.Drawing.Size(320, 20);
            this.edWinSCP.TabIndex = 11;
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(440, 11);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(26, 23);
            this.btnLog.TabIndex = 12;
            this.btnLog.Text = "...";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(440, 38);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(26, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edMailServer
            // 
            this.edMailServer.Location = new System.Drawing.Point(127, 69);
            this.edMailServer.Name = "edMailServer";
            this.edMailServer.Size = new System.Drawing.Size(320, 20);
            this.edMailServer.TabIndex = 14;
            // 
            // edUsername
            // 
            this.edUsername.Location = new System.Drawing.Point(127, 98);
            this.edUsername.Name = "edUsername";
            this.edUsername.Size = new System.Drawing.Size(320, 20);
            this.edUsername.TabIndex = 15;
            // 
            // edPassword
            // 
            this.edPassword.Location = new System.Drawing.Point(127, 127);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(320, 20);
            this.edPassword.TabIndex = 16;
            // 
            // edEmail
            // 
            this.edEmail.Location = new System.Drawing.Point(127, 156);
            this.edEmail.Name = "edEmail";
            this.edEmail.Size = new System.Drawing.Size(320, 20);
            this.edEmail.TabIndex = 17;
            // 
            // edSupport
            // 
            this.edSupport.Location = new System.Drawing.Point(127, 185);
            this.edSupport.Name = "edSupport";
            this.edSupport.Size = new System.Drawing.Size(320, 20);
            this.edSupport.TabIndex = 18;
            // 
            // edPort
            // 
            this.edPort.Location = new System.Drawing.Point(102, 214);
            this.edPort.Name = "edPort";
            this.edPort.Size = new System.Drawing.Size(46, 20);
            this.edPort.TabIndex = 19;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(367, 244);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(353, 221);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Polling Time (Minutes) ";
            // 
            // edPollTime
            // 
            this.edPollTime.Location = new System.Drawing.Point(472, 218);
            this.edPollTime.Name = "edPollTime";
            this.edPollTime.Size = new System.Drawing.Size(38, 20);
            this.edPollTime.TabIndex = 22;
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 279);
            this.Controls.Add(this.edPollTime);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.edPort);
            this.Controls.Add(this.edSupport);
            this.Controls.Add(this.edEmail);
            this.Controls.Add(this.edPassword);
            this.Controls.Add(this.edUsername);
            this.Controls.Add(this.edMailServer);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.edWinSCP);
            this.Controls.Add(this.edLog);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSettings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbNone;
        private System.Windows.Forms.RadioButton rbTls;
        private System.Windows.Forms.RadioButton rbSsl;
        private System.Windows.Forms.TextBox edLog;
        private System.Windows.Forms.TextBox edWinSCP;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edMailServer;
        private System.Windows.Forms.TextBox edUsername;
        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.TextBox edEmail;
        private System.Windows.Forms.TextBox edSupport;
        private System.Windows.Forms.TextBox edPort;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edPollTime;
    }
}