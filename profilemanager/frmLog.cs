﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace profilemanager
{
    public partial class frmLog : Form
    {

        string logFile;
        DataSet dsLog;

        public frmLog(string log)
        {
            InitializeComponent();
            this.logFile = log;
            this.Text = log;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmLog_Load(object sender, EventArgs e)
        {
            if (File.Exists(this.logFile))
            {
                dsLog = new DataSet();
                dsLog.ReadXml(this.logFile);
                BindingSource bs = new BindingSource();
                bs.DataSource = dsLog.Tables[0];
                grdLog.AutoGenerateColumns = false;
                grdLog.DataSource = bs;
            }
            else
            {
                this.Text = "Log File: " + this.logFile + " does not exist";
            }



        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            var row = dsLog.Tables[0].NewRow();
            dsLog.Tables[0].Clear();
            dsLog.Tables[0].Rows.Add(row);
            dsLog.AcceptChanges();
            dsLog.WriteXml(this.logFile);
        }



        private void grdLog_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == grdLog.Columns.Count - 1)
            {

            }
        }

        private void grdLog_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var selectedRow = grdLog.Rows[e.RowIndex];
                lblDateTime.Text = selectedRow.Cells["DateTime"].Value.ToString();
                lblFileName.Text = selectedRow.Cells["FileName"].Value.ToString();
                lblProcess.Text = selectedRow.Cells["Process"].Value.ToString();
                lblProfile.Text = selectedRow.Cells["ProfileID"].Value.ToString();
                lblResult.Text = selectedRow.Cells["Result"].Value.ToString();
                txtMessage.Text = selectedRow.Cells["Message"].Value.ToString();
            }
        }
    }
}

