﻿
namespace profilemanager
{
    partial class cwTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cwTest));
            this.btnClose = new System.Windows.Forms.Button();
            this.edResults = new System.Windows.Forms.RichTextBox();
            this.edPass = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.edUser = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.edUrl = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.edRecip = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(713, 354);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // edResults
            // 
            this.edResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edResults.Location = new System.Drawing.Point(13, 87);
            this.edResults.Name = "edResults";
            this.edResults.ReadOnly = true;
            this.edResults.Size = new System.Drawing.Size(775, 263);
            this.edResults.TabIndex = 1;
            this.edResults.Text = "";
            // 
            // edPass
            // 
            this.edPass.Location = new System.Drawing.Point(688, 12);
            this.edPass.Name = "edPass";
            this.edPass.Size = new System.Drawing.Size(100, 20);
            this.edPass.TabIndex = 43;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(622, 15);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 45;
            this.label14.Text = "Password";
            // 
            // edUser
            // 
            this.edUser.Location = new System.Drawing.Point(508, 12);
            this.edUser.Name = "edUser";
            this.edUser.Size = new System.Drawing.Size(100, 20);
            this.edUser.TabIndex = 41;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(442, 15);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 13);
            this.label15.TabIndex = 44;
            this.label15.Text = "User Name";
            // 
            // edUrl
            // 
            this.edUrl.Location = new System.Drawing.Point(91, 12);
            this.edUrl.Name = "edUrl";
            this.edUrl.Size = new System.Drawing.Size(316, 20);
            this.edUrl.TabIndex = 40;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 12);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 42;
            this.label16.Text = "eAdapter URL";
            // 
            // edRecip
            // 
            this.edRecip.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecip.Location = new System.Drawing.Point(171, 45);
            this.edRecip.Name = "edRecip";
            this.edRecip.Size = new System.Drawing.Size(89, 20);
            this.edRecip.TabIndex = 46;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(152, 13);
            this.label13.TabIndex = 47;
            this.label13.Text = "Cargowise Code (Recipient ID)";
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTest.Location = new System.Drawing.Point(632, 354);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 48;
            this.btnTest.Text = "&Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // cwTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 389);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.edRecip);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.edPass);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.edUser);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.edUrl);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.edResults);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "cwTest";
            this.Text = "Cargowise eAdapter Test";
            this.Load += new System.EventHandler(this.cwTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RichTextBox edResults;
        private System.Windows.Forms.TextBox edPass;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox edUser;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edUrl;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox edRecip;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnTest;
    }
}