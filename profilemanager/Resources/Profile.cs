﻿namespace profilemanager.Resources
{
    public class Profile
    {
        public int ID { get; set; }
        public string Customer { get; set; }
        public string Client { get; set; }
        public string CargowiseContext { get; set; }
        public string CargowiseServer { get; set; }
        public string OrgCode { get; set; }
        public string eAdapterURL { get; set; }
        public string eAdapterUsername { get; set; }
        public string eAdapterPassword { get; set; }
        public string FTPServer { get; set; }
        public string FTPUserName { get; set; }
        public string FTPPassword { get; set; }
        public string FTPPath { get; set; }
        public string ProcessedPath { get; set; }
        public string LogName { get; set; }
        public bool Active { get; set; }
    }
}
