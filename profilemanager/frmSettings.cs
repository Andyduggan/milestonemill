﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace profilemanager
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            edLog.Text = Globals.LogLoc;
            edWinSCP.Text = Globals.WinSCP;
            edMailServer.Text = Globals.MailServer.Server;
            edEmail.Text = Globals.MailServer.MailFrom;
            edPort.Text = Globals.MailServer.Port;
            edUsername.Text = Globals.MailServer.User;
            edPassword.Text = Globals.MailServer.Password;
            edSupport.Text = Globals.SupportEmail;
            if (Globals.PollTime == 0)
            {
                edPollTime.Text = "5";
            }
            else
            {
                edPollTime.Text = Globals.PollTime.ToString();
            }
            switch (Globals.MailServer.MailOption)
            {
                case "SSL":
                    rbSsl.Checked = true;
                    break;
                case "TLS":
                    rbTls.Checked = true;
                    break;
                case "None":
                    rbNone.Checked = true;
                    break;
                default:
                    rbNone.Checked = true;
                    break;
            }
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            var fb = new FolderBrowserDialog();
            fb.SelectedPath = edLog.Text;
            fb.ShowDialog();
            edLog.Text = fb.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var fi = new FolderBrowserDialog();
            fi.SelectedPath = edWinSCP.Text;
            fi.Description = "Enter path where WinSCP.EXE exists.";
            fi.ShowDialog();
            edWinSCP.Text = fi.SelectedPath;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var settings = XDocument.Load(Path.Combine(Globals.AppPath, "ProfileManager.XML"));
            var prefs = (from p in settings.Descendants("Preferences")
                         select p).FirstOrDefault();
            prefs.Element("LogPath").Value = edLog.Text;
            prefs.Element("WinSCPLocation").Value = edWinSCP.Text;
            prefs.Element("PollTime").Value = edPollTime.Text;
            prefs.Element("SupportEmail").Value = edSupport.Text;
            prefs.Element("MailServer").Value = edMailServer.Text;
            prefs.Element("Port").Value = edPort.Text;
            prefs.Element("SenderEmail").Value = edEmail.Text;
            prefs.Element("UserName").Value = edUsername.Text;
            prefs.Element("Password").Value = edPassword.Text;
            prefs.Element("ServerOptions").Value = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked).Text;
            prefs.Save(Path.Combine(Globals.AppPath, "ProfileManager.XML"));
        }
    }
}
