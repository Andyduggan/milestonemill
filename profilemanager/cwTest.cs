﻿using profilemanager.Resources;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace profilemanager
{
    public partial class cwTest : Form
    {
        public string Url { get; set; }
        public string User { get; set; }
        public string Pass { get; set; }
        public string Recip { get; set; }

        public cwTest(string url, string user, string pass, string recip)
        {
            InitializeComponent();
            Url = url;
            User = user;
            Pass = pass;
            Recip = recip;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cwTest_Load(object sender, EventArgs e)
        {
            edUrl.Text = Url;
            edUser.Text = User;
            edPass.Text = Pass;
            edRecip.Text = Recip;
            edResults.AppendText("Ready to Test", Color.Green, new Font("Microsoft Sans Serif", 9, FontStyle.Bold));
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            edResults.AppendText("Beginning Test", Color.Black, new Font("Microsoft Sans Serif", 9, FontStyle.Regular));
            if (edAdpterPing())
            {
                if (edapterSendFile())
                {

                }
                else
                {
                    edResults.AppendText("URL is correct. Check Username, Password and Recipient ID are Correct.", Color.Black, new Font("Microsoft Sans Serif", 9, FontStyle.Regular));
                }
            }
            else
            {
                edResults.AppendText("Ping Test Failed. Check eAdapter URL", Color.Black, new Font("Microsoft Sans Serif", 9, FontStyle.Regular));
            }
        }

        private bool edapterSendFile()
        {
            string sampleFile = Path.Combine(Globals.AppPath, "Sample.XML");
            if (File.Exists(sampleFile))
            {
                File.Delete(sampleFile);
            }

            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            var doc = new XDocument(new XDeclaration("1.0", "UTF-8", ""),
                                                new XElement(ns + "UniversalInterchange",
                                                new XAttribute("version", "1.1"),
                                                     new XElement("Header",
                                                         new XElement("SenderID", "FreightTracker"),
                                                         new XElement("RecipientID", edRecip.Text.ToUpper())),
                                                     new XElement("Body")));
            doc.Save(sampleFile);

            try
            {
                eAdaptor.SendMessage(
                    edUrl.Text,
                    sampleFile,
                    edRecip.Text,
                    edUser.Text,
                    edPass.Text);
                edResults.AppendText("Sample File Sent Ok.", Color.Green, new Font("Microsoft Sans Serif", 9, FontStyle.Bold));
                File.Delete(sampleFile);
            }
            catch (Exception ex)
            {
                edResults.AppendText(String.Format("Error during message sending: {0}", ex.Message), Color.Red, new Font("Microsoft Sans Serif", 9, FontStyle.Bold));
            }
            return true;
        }

        private bool edAdpterPing(bool SuppressSuccessMessages = false)
        {
            var serviceAddress = edUrl.Text;
            if (String.IsNullOrEmpty(serviceAddress))
            {
                edResults.AppendText("URL cannot be blank", Color.Orange, new Font("Microsoft Sans Serif", 9, FontStyle.Bold));
                return false;
            }
            if (!SuppressSuccessMessages)
            {
                edResults.AppendText(String.Format("Ping {0}", serviceAddress), Color.Black, new Font("Microsoft Sans Serif", 9, FontStyle.Regular));
            }
            try
            {
                if (eAdaptor.Ping(serviceAddress, edUser.Text, edPass.Text))
                {
                    if (!SuppressSuccessMessages) edResults.AppendText("Ping ok.", Color.Black, new Font("Microsoft Sans Serif", 9, FontStyle.Regular));
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                edResults.AppendText(String.Format("Ping error: {0}", ex.Message), Color.Red, new Font("Microsoft Sans Serif", 9, FontStyle.Bold));
                return false;
            }
        }

    }


}
