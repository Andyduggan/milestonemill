﻿namespace profilemanager
{
    public static class Globals
    {
        public static string Profiles { get; set; }
        public static string LogLoc { get; set; }
        public static string WinSCP { get; set; }
        public static string SupportEmail { get; set; }
        public static MailServer MailServer { get; set; }

        public const string AppPath = @"C:\30KFT";
        public static int PollTime { get; set; }

    }

    public class MailServer
    {
        public string Server { get; set; }
        public string MailOption { get; set; }
        public string Port { get; set; }
        public string MailFrom { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public MailServer()
        {

        }
    }
}
