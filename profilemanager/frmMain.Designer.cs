﻿
namespace profilemanager
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewSystemLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grdProfiles = new System.Windows.Forms.DataGridView();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Client = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrgCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CargowiseContext = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CargowiseServer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eAdapterURL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LogName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eAdapterUsername = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eAdapterPassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTPServer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTPUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTPPassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTPPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProcessedPath = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.edCustomer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.edCWContext = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edClient = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.edCWServer = new System.Windows.Forms.TextBox();
            this.edEadapter = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.edUser = new System.Windows.Forms.TextBox();
            this.edPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edFtpServer = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edFTPPass = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.edFTPUser = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.edPath = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.edProcessedPath = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnViewLog = new System.Windows.Forms.Button();
            this.edCWCode = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.rbActive = new System.Windows.Forms.RadioButton();
            this.rbInactive = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProfiles)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(806, 424);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.systemToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(893, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.viewSystemLogToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.systemToolStripMenuItem.Text = "S&ystem";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.settingsToolStripMenuItem.Text = "Se&ttings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // viewSystemLogToolStripMenuItem
            // 
            this.viewSystemLogToolStripMenuItem.Name = "viewSystemLogToolStripMenuItem";
            this.viewSystemLogToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.viewSystemLogToolStripMenuItem.Text = "View System Log";
            this.viewSystemLogToolStripMenuItem.Click += new System.EventHandler(this.viewSystemLogToolStripMenuItem_Click);
            // 
            // grdProfiles
            // 
            this.grdProfiles.AllowUserToAddRows = false;
            this.grdProfiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdProfiles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Customer,
            this.Client,
            this.OrgCode,
            this.CargowiseContext,
            this.CargowiseServer,
            this.eAdapterURL,
            this.LogName,
            this.Active,
            this.ID,
            this.eAdapterUsername,
            this.eAdapterPassword,
            this.FTPServer,
            this.FTPUserName,
            this.FTPPassword,
            this.FTPPath,
            this.ProcessedPath});
            this.grdProfiles.Location = new System.Drawing.Point(13, 48);
            this.grdProfiles.Name = "grdProfiles";
            this.grdProfiles.ReadOnly = true;
            this.grdProfiles.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdProfiles.Size = new System.Drawing.Size(868, 204);
            this.grdProfiles.TabIndex = 2;
            this.grdProfiles.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.grdProfiles_CellMouseClick);
            this.grdProfiles.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.grdProfiles_DataError);
            // 
            // Customer
            // 
            this.Customer.DataPropertyName = "Customer";
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            this.Customer.Width = 200;
            // 
            // Client
            // 
            this.Client.DataPropertyName = "Client";
            this.Client.HeaderText = "Client";
            this.Client.Name = "Client";
            this.Client.ReadOnly = true;
            this.Client.Width = 200;
            // 
            // OrgCode
            // 
            this.OrgCode.DataPropertyName = "OrgCode";
            this.OrgCode.HeaderText = "CW Code";
            this.OrgCode.Name = "OrgCode";
            this.OrgCode.ReadOnly = true;
            // 
            // CargowiseContext
            // 
            this.CargowiseContext.DataPropertyName = "CargowiseContext";
            this.CargowiseContext.HeaderText = "Context";
            this.CargowiseContext.Name = "CargowiseContext";
            this.CargowiseContext.ReadOnly = true;
            this.CargowiseContext.Visible = false;
            // 
            // CargowiseServer
            // 
            this.CargowiseServer.DataPropertyName = "CargowiseServer";
            this.CargowiseServer.HeaderText = "Server";
            this.CargowiseServer.Name = "CargowiseServer";
            this.CargowiseServer.ReadOnly = true;
            this.CargowiseServer.Visible = false;
            // 
            // eAdapterURL
            // 
            this.eAdapterURL.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.eAdapterURL.DataPropertyName = "eAdapterURL";
            this.eAdapterURL.HeaderText = "eAdapter URL";
            this.eAdapterURL.Name = "eAdapterURL";
            this.eAdapterURL.ReadOnly = true;
            // 
            // LogName
            // 
            this.LogName.DataPropertyName = "LogName";
            this.LogName.HeaderText = "Log Name";
            this.LogName.Name = "LogName";
            this.LogName.ReadOnly = true;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "Active";
            this.Active.FalseValue = "0";
            this.Active.HeaderText = "Active";
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.TrueValue = "1";
            this.Active.Width = 50;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Visible = false;
            this.ID.Width = 50;
            // 
            // eAdapterUsername
            // 
            this.eAdapterUsername.DataPropertyName = "eAdapterUsername";
            this.eAdapterUsername.HeaderText = "User";
            this.eAdapterUsername.Name = "eAdapterUsername";
            this.eAdapterUsername.ReadOnly = true;
            this.eAdapterUsername.Visible = false;
            // 
            // eAdapterPassword
            // 
            this.eAdapterPassword.DataPropertyName = "eAdapterPassword";
            this.eAdapterPassword.HeaderText = "Password";
            this.eAdapterPassword.Name = "eAdapterPassword";
            this.eAdapterPassword.ReadOnly = true;
            this.eAdapterPassword.Visible = false;
            // 
            // FTPServer
            // 
            this.FTPServer.DataPropertyName = "FTPServer";
            this.FTPServer.HeaderText = "FTPServer";
            this.FTPServer.Name = "FTPServer";
            this.FTPServer.ReadOnly = true;
            this.FTPServer.Visible = false;
            // 
            // FTPUserName
            // 
            this.FTPUserName.DataPropertyName = "FTPUserName";
            this.FTPUserName.HeaderText = "FTPUser";
            this.FTPUserName.Name = "FTPUserName";
            this.FTPUserName.ReadOnly = true;
            this.FTPUserName.Visible = false;
            // 
            // FTPPassword
            // 
            this.FTPPassword.DataPropertyName = "FTPPassword";
            this.FTPPassword.HeaderText = "FTPPassword";
            this.FTPPassword.Name = "FTPPassword";
            this.FTPPassword.ReadOnly = true;
            this.FTPPassword.Visible = false;
            // 
            // FTPPath
            // 
            this.FTPPath.DataPropertyName = "FTPPath";
            this.FTPPath.HeaderText = "FTPPath";
            this.FTPPath.Name = "FTPPath";
            this.FTPPath.ReadOnly = true;
            this.FTPPath.Visible = false;
            // 
            // ProcessedPath
            // 
            this.ProcessedPath.DataPropertyName = "ProcessedPath";
            this.ProcessedPath.HeaderText = "ProcessedPath";
            this.ProcessedPath.Name = "ProcessedPath";
            this.ProcessedPath.ReadOnly = true;
            this.ProcessedPath.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Customer Name";
            // 
            // edCustomer
            // 
            this.edCustomer.Location = new System.Drawing.Point(94, 267);
            this.edCustomer.Name = "edCustomer";
            this.edCustomer.Size = new System.Drawing.Size(316, 20);
            this.edCustomer.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(424, 268);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cargowise Context";
            // 
            // edCWContext
            // 
            this.edCWContext.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCWContext.Location = new System.Drawing.Point(526, 266);
            this.edCWContext.Name = "edCWContext";
            this.edCWContext.Size = new System.Drawing.Size(41, 20);
            this.edCWContext.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Client Name";
            // 
            // edClient
            // 
            this.edClient.Location = new System.Drawing.Point(94, 303);
            this.edClient.Name = "edClient";
            this.edClient.Size = new System.Drawing.Size(316, 20);
            this.edClient.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(425, 306);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Cargowise Server";
            // 
            // edCWServer
            // 
            this.edCWServer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCWServer.Location = new System.Drawing.Point(526, 303);
            this.edCWServer.Name = "edCWServer";
            this.edCWServer.Size = new System.Drawing.Size(39, 20);
            this.edCWServer.TabIndex = 8;
            // 
            // edEadapter
            // 
            this.edEadapter.Location = new System.Drawing.Point(94, 339);
            this.edEadapter.Name = "edEadapter";
            this.edEadapter.Size = new System.Drawing.Size(316, 20);
            this.edEadapter.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 339);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "eAdapter URL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(445, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "User Name";
            // 
            // edUser
            // 
            this.edUser.Location = new System.Drawing.Point(511, 339);
            this.edUser.Name = "edUser";
            this.edUser.Size = new System.Drawing.Size(100, 20);
            this.edUser.TabIndex = 11;
            // 
            // edPassword
            // 
            this.edPassword.Location = new System.Drawing.Point(691, 339);
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(100, 20);
            this.edPassword.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(625, 342);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Password";
            // 
            // edFtpServer
            // 
            this.edFtpServer.Location = new System.Drawing.Point(94, 375);
            this.edFtpServer.Name = "edFtpServer";
            this.edFtpServer.Size = new System.Drawing.Size(316, 20);
            this.edFtpServer.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 378);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "FTP Server";
            // 
            // edFTPPass
            // 
            this.edFTPPass.Location = new System.Drawing.Point(691, 371);
            this.edFTPPass.Name = "edFTPPass";
            this.edFTPPass.Size = new System.Drawing.Size(100, 20);
            this.edFTPPass.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(625, 374);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Password";
            // 
            // edFTPUser
            // 
            this.edFTPUser.Location = new System.Drawing.Point(511, 371);
            this.edFTPUser.Name = "edFTPUser";
            this.edFTPUser.Size = new System.Drawing.Size(100, 20);
            this.edFTPUser.TabIndex = 14;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(445, 374);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "User Name";
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(821, 296);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(60, 23);
            this.btnTest.TabIndex = 23;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(806, 368);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 18;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // edPath
            // 
            this.edPath.Location = new System.Drawing.Point(94, 410);
            this.edPath.Name = "edPath";
            this.edPath.Size = new System.Drawing.Size(254, 20);
            this.edPath.TabIndex = 16;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 413);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Milestone Path";
            // 
            // edProcessedPath
            // 
            this.edProcessedPath.Location = new System.Drawing.Point(475, 409);
            this.edProcessedPath.Name = "edProcessedPath";
            this.edProcessedPath.Size = new System.Drawing.Size(316, 20);
            this.edProcessedPath.TabIndex = 17;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(387, 412);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Processed Path";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.Location = new System.Drawing.Point(806, 395);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 19;
            this.btnNew.Text = "&New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnViewLog
            // 
            this.btnViewLog.Location = new System.Drawing.Point(806, 258);
            this.btnViewLog.Name = "btnViewLog";
            this.btnViewLog.Size = new System.Drawing.Size(75, 23);
            this.btnViewLog.TabIndex = 30;
            this.btnViewLog.Text = "&View Log";
            this.btnViewLog.UseVisualStyleBackColor = true;
            this.btnViewLog.Click += new System.EventHandler(this.btnViewLog_Click);
            // 
            // edCWCode
            // 
            this.edCWCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCWCode.Location = new System.Drawing.Point(689, 303);
            this.edCWCode.Name = "edCWCode";
            this.edCWCode.Size = new System.Drawing.Size(89, 20);
            this.edCWCode.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(588, 306);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Cargowise Code";
            // 
            // cbActive
            // 
            this.cbActive.AutoSize = true;
            this.cbActive.Location = new System.Drawing.Point(598, 270);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(88, 17);
            this.cbActive.TabIndex = 6;
            this.cbActive.Text = "Profile Active";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // rbActive
            // 
            this.rbActive.AutoSize = true;
            this.rbActive.Location = new System.Drawing.Point(110, 25);
            this.rbActive.Name = "rbActive";
            this.rbActive.Size = new System.Drawing.Size(85, 17);
            this.rbActive.TabIndex = 32;
            this.rbActive.TabStop = true;
            this.rbActive.Text = "Show Active";
            this.rbActive.UseVisualStyleBackColor = true;
            this.rbActive.CheckedChanged += new System.EventHandler(this.rbActive_CheckedChanged);
            // 
            // rbInactive
            // 
            this.rbInactive.AutoSize = true;
            this.rbInactive.Location = new System.Drawing.Point(221, 25);
            this.rbInactive.Name = "rbInactive";
            this.rbInactive.Size = new System.Drawing.Size(93, 17);
            this.rbInactive.TabIndex = 33;
            this.rbInactive.TabStop = true;
            this.rbInactive.Text = "Show Inactive";
            this.rbInactive.UseVisualStyleBackColor = true;
            this.rbInactive.CheckedChanged += new System.EventHandler(this.rbInactive_CheckedChanged);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 460);
            this.Controls.Add(this.rbInactive);
            this.Controls.Add(this.rbActive);
            this.Controls.Add(this.cbActive);
            this.Controls.Add(this.edCWCode);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnViewLog);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.edProcessedPath);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.edPath);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.edFTPPass);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.edFTPUser);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.edFtpServer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.edPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.edUser);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.edEadapter);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.edCWServer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.edClient);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.edCWContext);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edCustomer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grdProfiles);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.Text = "Profile Manager";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdProfiles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.DataGridView grdProfiles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edCustomer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edCWContext;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edClient;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox edCWServer;
        private System.Windows.Forms.TextBox edEadapter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox edUser;
        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edFtpServer;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edFTPPass;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox edFTPUser;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox edPath;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edProcessedPath;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnViewLog;
        private System.Windows.Forms.ToolStripMenuItem viewSystemLogToolStripMenuItem;
        private System.Windows.Forms.TextBox edCWCode;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Client;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrgCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn CargowiseContext;
        private System.Windows.Forms.DataGridViewTextBoxColumn CargowiseServer;
        private System.Windows.Forms.DataGridViewTextBoxColumn eAdapterURL;
        private System.Windows.Forms.DataGridViewTextBoxColumn LogName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn eAdapterUsername;
        private System.Windows.Forms.DataGridViewTextBoxColumn eAdapterPassword;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTPServer;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTPUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTPPassword;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTPPath;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProcessedPath;
        private System.Windows.Forms.RadioButton rbActive;
        private System.Windows.Forms.RadioButton rbInactive;
    }
}

