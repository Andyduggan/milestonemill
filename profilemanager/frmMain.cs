﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace profilemanager
{
    public partial class frmMain : Form
    {
        int profileId;
        DataSet dsProfiles;
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var settings = new frmSettings();
            var setSaved = settings.ShowDialog();
            if (setSaved == DialogResult.OK)
            {
                LoadSettings();
            }
        }
        private void LoadSettings()
        {
            if (!Directory.Exists(Globals.AppPath))
            {
                Directory.CreateDirectory(Globals.AppPath);
            }
            if (!File.Exists(Path.Combine(Globals.AppPath, "ProfileManager.XML")))
            {
                XDocument prefs = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                    new XElement("Preferences",
                                                    new XElement("Profiles"),
                                                    new XElement("LogPath"),
                                                    new XElement("WinSCPLocation"),
                                                    new XElement("PollTime"),
                                                    new XElement("SupportEmail"),
                                                    new XElement("MailServer"),
                                                    new XElement("Port"),
                                                    new XElement("SenderEmail"),
                                                    new XElement("UserName"),
                                                    new XElement("Password"),
                                                    new XElement("ServerOptions")));
                prefs.Save(Path.Combine(Globals.AppPath, "ProfileManager.XML"));
            }
            XDocument pmPrefs = XDocument.Load(Path.Combine(Globals.AppPath, "ProfileManager.XML"));
            var settings = (from p in pmPrefs.Descendants("Preferences")
                            select p).FirstOrDefault();
            Globals.LogLoc = settings.Element("LogPath").Value;


            if (settings.Element("Profiles") == null)
            {
                XElement ele = new XElement("Profiles");
                ele.Value = @"C:\30Kft\Profiles.XML";
                settings.Add(ele);
            }
            Globals.Profiles = settings.Element("Profiles").Value;
            Globals.MailServer = new MailServer
            {
                MailFrom = settings.Element("SenderEmail").Value,
                Server = settings.Element("MailServer").Value,
                Port = settings.Element("Port").Value,
                User = settings.Element("UserName").Value,
                Password = settings.Element("Password").Value,
                MailOption = settings.Element("ServerOptions").Value
            };
            Globals.SupportEmail = settings.Element("SupportEmail").Value;
            int poll = 0;
            if (int.TryParse(settings.Element("PollTime").Value, out poll))
            {
                Globals.PollTime = poll;
            }
            else
            {
                Globals.PollTime = 5;
            }
            pmPrefs.Save(Path.Combine(Globals.AppPath, "ProfileManager.XML"));


        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadSettings();
            LoadProfiles();
            rbActive.Checked = true;
        }

        private void LoadProfiles()
        {
            if (!File.Exists(Path.Combine(Globals.AppPath, "Profile.XML")))
            {
                XDocument defaultProfile = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                                                    new XElement("Profiles",
                                                    new XElement("Profile",
                                                        new XElement("ID"),
                                                        new XElement("Customer"),
                                                        new XElement("Client"),
                                                        new XElement("CargowiseContext"),
                                                        new XElement("CargowiseServer"),
                                                        new XElement("OrgCode"),
                                                        new XElement("eAdapterURL"),
                                                        new XElement("eAdapterUsername"),
                                                        new XElement("eAdapterPassword"),
                                                        new XElement("FTPServer"),
                                                        new XElement("FTPUserName"),
                                                        new XElement("FTPPassword"),
                                                        new XElement("FTPPath"),
                                                        new XElement("ProcessedPath"),
                                                        new XElement("LogName"),
                                                        new XElement("Active")
                                                        )
                                                    )
                                                    );
                defaultProfile.Save(Path.Combine(Globals.AppPath, "Profile.XML"));
            }
            using (var xmlReader = XmlReader.Create(Path.Combine(Globals.AppPath, "Profile.XML")))
            {
                dsProfiles = new DataSet();
                dsProfiles.ReadXml(xmlReader);
                BindingSource bs = new BindingSource();
                bs.DataSource = dsProfiles.Tables[0];
                string filt = rbActive.Checked ? "1" : "0";
                bs.Filter = "Active='" + filt + "'";
                grdProfiles.AutoGenerateColumns = false;
                grdProfiles.DataSource = bs;

            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            this.Controls.OfType<TextBox>().ToList().ForEach(t => t.Text = string.Empty);
            btnSave.Text = "&Save";
            profileId = GetNextProfileId();
            edCustomer.Focus();
        }

        private int GetNextProfileId()
        {
            int id = 0;
            foreach (DataRow row in dsProfiles.Tables[0].Rows)
            {
                int i;

                if (int.TryParse(row.Field<string>("ID"), out i))
                {
                    id = Math.Max(id, i);
                }

            }
            return id + 1;
        }

        private void grdProfiles_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (e.ColumnIndex == grdProfiles.Columns.Count - 1)
            {

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "&Save")
            {
                var row = dsProfiles.Tables[0].NewRow();
                row["Customer"] = edCustomer.Text;
                row["Client"] = edClient.Text;
                row["CargowiseContext"] = edCWContext.Text;
                row["CargowiseServer"] = edCWServer.Text;
                row["OrgCode"] = edCWCode.Text;
                row["eAdapterURL"] = edEadapter.Text;
                row["eAdapterUsername"] = edUser.Text;
                row["eAdapterPassword"] = edPassword.Text;
                row["FTPServer"] = edFtpServer.Text;
                row["FTPUserName"] = edFTPUser.Text;
                row["ID"] = profileId.ToString();
                row["Active"] = cbActive.Checked ? "1" : "0";
                row["FTPPassword"] = edFTPPass.Text;
                row["FTPPath"] = edPath.Text;
                row["ProcessedPath"] = edProcessedPath.Text;
                row["LogName"] = edCWContext.Text + edCWServer.Text + ".XML";
                dsProfiles.Tables[0].Rows.Add(row);
            }
            else if (btnSave.Text == "&Update")
            {
                var row = dsProfiles.Tables[0].Select("ID = '" + profileId.ToString() + "'").FirstOrDefault();
                row["Customer"] = edCustomer.Text;
                row["Client"] = edClient.Text;
                row["CargowiseContext"] = edCWContext.Text;
                row["CargowiseServer"] = edCWServer.Text;
                row["OrgCode"] = edCWCode.Text;
                row["eAdapterURL"] = edEadapter.Text;
                row["eAdapterUsername"] = edUser.Text;
                row["eAdapterPassword"] = edPassword.Text;
                row["FTPServer"] = edFtpServer.Text;
                row["FTPUserName"] = edFTPUser.Text;
                row["ID"] = profileId.ToString();
                row["Active"] = cbActive.Checked ? "1" : "0";
                row["FTPPassword"] = edFTPPass.Text;
                row["FTPPath"] = edPath.Text;
                row["ProcessedPath"] = edProcessedPath.Text;
                row["LogName"] = edCWContext.Text + edCWServer.Text + ".XML";
            }


            var empty = dsProfiles.Tables[0].Select("ID=''");
            foreach (var row in empty)
            {
                dsProfiles.Tables[0].Rows.Remove(row);
            }
            dsProfiles.AcceptChanges();
            dsProfiles.WriteXml(Path.Combine(Globals.AppPath, "Profile.XML"));
            LoadProfiles();
            btnSave.Text = "&Save";
        }

        private void grdProfiles_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var selectedRow = grdProfiles.Rows[e.RowIndex];
                edCustomer.Text = selectedRow.Cells["Customer"].Value.ToString();
                edClient.Text = selectedRow.Cells["Client"].Value.ToString();
                edCWContext.Text = selectedRow.Cells["CargowiseContext"].Value.ToString();
                edCWServer.Text = selectedRow.Cells["CargowiseServer"].Value.ToString();
                edCWCode.Text = selectedRow.Cells["OrgCode"].Value.ToString();
                edEadapter.Text = selectedRow.Cells["eAdapterURL"].Value.ToString();
                edUser.Text = selectedRow.Cells["eAdapterUsername"].Value.ToString();
                edPassword.Text = selectedRow.Cells["eAdapterPassword"].Value.ToString();
                profileId = string.IsNullOrEmpty(selectedRow.Cells["ID"].Value.ToString()) ? 1 : int.Parse(selectedRow.Cells["ID"].Value.ToString());
                edFtpServer.Text = selectedRow.Cells["FTPServer"].Value.ToString();
                edFTPUser.Text = selectedRow.Cells["FTPUserName"].Value.ToString();
                edFTPPass.Text = selectedRow.Cells["FTPPassword"].Value.ToString();
                edPath.Text = selectedRow.Cells["FTPPath"].Value.ToString();
                edProcessedPath.Text = selectedRow.Cells["ProcessedPath"].Value.ToString();
                cbActive.Checked = selectedRow.Cells["Active"].Value.ToString() == "1" ? true : false;
                btnSave.Text = "&Update";

            }
        }

        private void rbInactive_CheckedChanged(object sender, EventArgs e)
        {
            LoadProfiles();
        }

        private void rbActive_CheckedChanged(object sender, EventArgs e)
        {
            LoadProfiles();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            var cwtest = new cwTest(edEadapter.Text, edUser.Text, edPassword.Text, edCWCode.Text);
            cwtest.Show();


        }

        private void viewSystemLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var logviewer = new frmLog(Path.Combine(Globals.LogLoc, "MileStoneMillLog.xml"));
            logviewer.Show();
        }

        private void btnViewLog_Click(object sender, EventArgs e)
        {
            if (btnSave.Text == "&Update")
            {
                var logviewer = new frmLog(Path.Combine(Globals.LogLoc, edCWContext.Text + edCWServer.Text + ".XML"));
                logviewer.Show();
            }

        }
    }
}
