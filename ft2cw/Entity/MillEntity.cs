using System.Data.Entity;

namespace ft2cw.Models
{
    public class MillEntity : DbContext
    {
        // Your context has been configured to use a 'MillEntity' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'ft2cw.Entity.MillEntity' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'MillEntity' 
        // connection string in the application configuration file.
        public MillEntity()
            : base("name=MillEntity")
        {
        }

        public virtual DbSet<Profile> Profiles { get; set; }
        public virtual DbSet<Mill_Log> Logs { get; set; }
        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Profile>()
                 .ToTable("profile");
            modelBuilder.Entity<Mill_Log>().ToTable("log");
        }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}